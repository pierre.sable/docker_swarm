<?php
  $color= getenv('COLOR') ? getenv('COLOR') : "orange";
  $backend_name = getenv('BACKEND') ? getenv('BACKEND') : "backend";
  $backend_url="http://".$backend_name."/index.php";

  function get_hostname() {
    global $color;
    return "<font color=\"" . $color . "\">" . gethostname() . "</font>";
  }

  function call_backend() {
    global $backend_url;
    $worker = file_get_contents($backend_url);
    return $worker;
  }
?>
<html>
  <body>
    <h1> Traitement frontend : <?php echo get_hostname(); ?></h1>
    <h1> Traitement backend  : <?php echo call_backend(); ?></h1>
  </body>
</html>
